// Створити клас Employee, у якому будуть такі характеристики - name(ім'я), age (вік), salary (зарплата). Зробіть так,
// щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang(список мов).
// Для класу Programmer перезапишіть гетер для властивості salary.Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


// Create class Employee
class Employee {
    constructor(name = '', age = 18, salary = 0) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    // Method to show error when enter invalid data
    showError(invalidValue) {
        console.error(`You enter invalid value - "${invalidValue}"`)
    }
    // Accessor properties for NAME
    get name() {
        return this._name
    }
    set name(str) {
        str.length > 2 ? this._name = str : this.showError(str)
    }
    // Accessor properties for AGE
    get age() {
        return this.age
    }
    set age(num) {
        !isNaN(num) && num >= 18 ? this._age = num : this.showError(num)
    }
    // Accessor properties for SALARY
    get salary() {
        return this._salary
    }
    set salary(num) {
        !isNaN(num) && num > 0 ? this._salary = num : this.showError(num)
    }
}

class Programmer extends Employee {
    constructor(name = '', age = 18, salary = 0, lang = []) {
        super(name, age, salary);
        this._lang = lang;
    }
    // Accessor properties for SALARY (rewrited)
    get salary() {
        return super.salary * 3
        // return this._salary * 3
    }
    set salary(num) {
        super.salary = num
        // !isNaN(num) && num > 0 ? this._salary = num : this.showError(num)
    }
    // Accessor properties for LANG
    get lang() {
        return this._lang
    }
    set lang(str) {
        if (typeof str === 'string' && str.length > 1 && isNaN(str)) {
            this._lang.push(str)
        } else if (Array.isArray(str) && str.every(a => isNaN(a))) {
            this._lang = [...new Set([...this._lang, ...str])]
        } else {
            this.showError(str)
        }
    }
}
// Create object instances
const progerJun = new Programmer('Vasia', 20, 200, ['ua', 'ru']);
const progerMid = new Programmer('Biba', 26, 1200, ['ua', 'ru', 'eng']);
const progerSin = new Programmer('Boba', 30, 3200, ['ua', 'ru', 'eng', 'pl']);
// Log object instances
console.log(progerJun);
console.log(progerMid);
console.log(progerSin);

//  
// console.log(progerJun.lang = 'e');
// console.log(progerJun.lang = 'eng');
// console.log(progerJun);